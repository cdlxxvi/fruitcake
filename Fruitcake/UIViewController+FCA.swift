//
//  UIViewController+FCA.swift
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 09/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


import UIKit

import FruitcakeFramework


private var StartTimestampAssociationKey: UnsafePointer<Void> = nil

extension UIViewController {
    var startTimestamp: NSTimeInterval? {
        get {
            return objc_getAssociatedObject(self, &StartTimestampAssociationKey) as? NSTimeInterval
        }
        set(timestamp) {
            objc_setAssociatedObject(self, &StartTimestampAssociationKey, timestamp, objc_AssociationPolicy(OBJC_ASSOCIATION_RETAIN_NONATOMIC))
        }
    }
    
    func logDisplay() {
        let duration = NSDate.timeIntervalSinceReferenceDate() - self.startTimestamp!
        let message = "\(duration * 1000)"
        FCALogger.logEvent(.Display, message: message, synchronous: false)
    }
}