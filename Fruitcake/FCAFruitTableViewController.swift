//
//  FCAFruitTableViewController.swift
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 08/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


import UIKit

import FruitcakeFramework


class FCAFruitTableViewController: UITableViewController {
    
    @IBOutlet private var _refreshBarButton: UIBarButtonItem!
    
    
    @IBAction private func _refresh(sender: AnyObject) {
        FCADataStore.sharedStore().reload()
    }
    
    
    deinit {
        FCADataStore.sharedStore().removeObserver(self, forKeyPath: "state")
    }
    
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        if (!object.isEqual(FCADataStore.sharedStore()) || keyPath != "state") {
            return
        }
        
        let state = FCADataStore.sharedStore().state
        switch state {
        case .Ready:
            FCADataStore.sharedStore().writeToCache()
            fallthrough
        case .Cached:
            self.navigationItem.rightBarButtonItem = self._refreshBarButton
            self.tableView.reloadData()

        case .Error:
            let title = NSLocalizedString("RefreshErrorTitle", comment: "")
            let message = NSLocalizedString("RefreshErrorMessage", comment: "")
            let buttonTitle = NSLocalizedString("OK", comment: "")
            let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: buttonTitle)
            alert.show()
            self.navigationItem.rightBarButtonItem = self._refreshBarButton

        case .Loading:
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
            activityIndicator.color = self.navigationController?.navigationBar.tintColor
            activityIndicator.startAnimating()
            let barButton = UIBarButtonItem(customView: activityIndicator)
            self.navigationItem.rightBarButtonItem = barButton
            
        default:
            break
        }
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        logDisplay()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("FruitListTitle", comment: "")

        let options = NSKeyValueObservingOptions.Initial | .New
        FCADataStore.sharedStore().addObserver(self, forKeyPath: "state", options: options, context: nil)
        
        FCADataStore.sharedStore().readFromCache()
        FCADataStore.sharedStore().reload()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let fruit = FCADataStore.sharedStore().fruit {
            return fruit.count
        }
        return 0
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier") as! UITableViewCell

        let fruit = FCADataStore.sharedStore().fruit[indexPath.row] as! FCAFruit
        cell.textLabel?.text = fruit.name

        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let cell = sender as! UITableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
        
        let fruitViewController = segue.destinationViewController as! FCAFruitDetailsTableViewController
        fruitViewController.fruit = FCADataStore.sharedStore().fruit![indexPath!.row] as? FCAFruit
    }
}
