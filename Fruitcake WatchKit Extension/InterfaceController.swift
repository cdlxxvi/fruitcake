//
//  InterfaceController.swift
//  Fruitcake WatchKit Extension
//
//  Created by Jacek Błaszkowski on 09/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


import WatchKit
import Foundation

import FruitcakeFramework


class InterfaceController: WKInterfaceController {

    @IBOutlet var _table: WKInterfaceTable!

    @IBAction func _reload() {
        FCADataStore.sharedStore().reload()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        let options = NSKeyValueObservingOptions.Initial | .New
        FCADataStore.sharedStore().addObserver(self, forKeyPath: "state", options: options, context: nil)
        
        FCADataStore.sharedStore().readFromCache()
        FCADataStore.sharedStore().reload()
    }
    
    
    deinit {
        FCADataStore.sharedStore().removeObserver(self, forKeyPath: "state")
    }
    
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        if (!object.isEqual(FCADataStore.sharedStore()) || keyPath != "state") {
            return
        }
        
        let fruit = FCADataStore.sharedStore().fruit
        if (fruit == nil) {
            return
        }
        _table.setNumberOfRows(fruit.count, withRowType: "FruitRow")
        for (i, fruit) in enumerate(fruit) {
            if let row = _table.rowControllerAtIndex(i) as? FCAFruitRow {
                row.nameLabel.setText(fruit.name)
                row.priceLabel.setText(fruit.formattedPrice)
                row.weightLabel.setText(fruit.formattedWeight)
            }
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
