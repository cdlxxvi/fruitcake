//
//  FCAFruitRow.swift
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 09/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


import Foundation
import WatchKit


class FCAFruitRow: NSObject {
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBOutlet weak var priceLabel: WKInterfaceLabel!
    @IBOutlet weak var weightLabel: WKInterfaceLabel!
}