//
//  FruitcakeFrameworkTests.swift
//  FruitcakeFrameworkTests
//
//  Created by Jacek Błaszkowski on 07/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


import UIKit
import XCTest

import FruitcakeFramework

class FruitcakeFrameworkTests: XCTestCase {
    
    let dataStore = FCADataStore.sharedStore()
    
    
    override func setUp() {
        super.setUp()

        let options: NSKeyValueObservingOptions = .Initial | .New
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDataStore() {
        // To start with, the data store should be in empty state.
        XCTAssertEqual(dataStore.state, .Empty, "Data store not initially empty")
        
        dataStore.readFromCache()
        
        // We will wait for the data store to load.
        let expectation = keyValueObservingExpectationForObject(dataStore, keyPath: "state") { (object, change) -> Bool in
            
            if (self.dataStore.state == .Ready) {
                self.dataStore.writeToCache()
            }
            
            return self.dataStore.state == .Ready
        }
        
        // After firing off a request the store should be in loading state.
        dataStore.reload()
        XCTAssertEqual(dataStore.state, .Loading, "Data store not in loading state after commencing loading")
        
        waitForExpectationsWithTimeout(10, handler:nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
}
