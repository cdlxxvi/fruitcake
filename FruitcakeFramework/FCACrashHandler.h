//
//  FCACrashHandler.h
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 09/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import <Foundation/Foundation.h>


/*!
 @brief Exceptions/signal handler.
 */
@interface FCACrashHandler : NSObject

/*!
 @brief Shared instance.
 @note  This is not a singleton. Other instances can be created as needed.
 
 @return    Shared handler instance.
 */
+ (FCACrashHandler *)sharedHandler;

/*!
 @brief Install the handler.
 
 @param handleExceptions    Handle exceptions?
 @param signals             Array of NSNumber-wrapped exception codes.
 */
- (void)installForExceptions:(BOOL)handleExceptions signals:(NSArray *)signals;

@end
