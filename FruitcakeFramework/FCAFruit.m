//
//  FCAFruit.m
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 07/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import "FCAFruit.h"

#import "FCAConstants.h"


static NSNumberFormatter *CurrencyFormatter = nil;


@implementation FCAFruit

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (! self) return nil;
    
    if (! CurrencyFormatter) {
        CurrencyFormatter = [[NSNumberFormatter alloc] init];
        CurrencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
        CurrencyFormatter.currencySymbol = @"£";
    }
    NSNumber *priceNumber = dictionary[FCADataPriceKey];
    float price = priceNumber.floatValue / 100;
    _formattedPrice = [CurrencyFormatter stringFromNumber:@(price)];
    
    NSString *name = dictionary[FCADataTypeKey];
    _name = [name capitalizedString];
    
    NSNumber *weightNumber = dictionary[FCADataWeightKey];
    float weight = weightNumber.floatValue / 1000;
    _formattedWeight = [NSString stringWithFormat:@"%.2fkg", weight];
    
    return self;
}

@end
