//
//  FCADataStore.h
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 07/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import <Foundation/Foundation.h>


/*!
 @brief The current state of the data store.
 */
typedef NS_ENUM(NSInteger, FCADataStoreState) {
    FCADataStoreStateEmpty,     //! @enum   The data store is empty.
    FCADataStoreStateCached,    //! @enum   The data store contains cached values.
    FCADataStoreStateError,     //! @enum   The data store failed to load data.
    FCADataStoreStateLoading,   //! @enum   The data store is loading data.
    FCADataStoreStateReady,     //! @enum   The data store has loaded data.
};


/*!
 @brief Fruitcake app data model class.
 */
@interface FCADataStore : NSObject

//! @brief  Error details. Set if the store is in error state.
@property (strong, nonatomic, readonly) NSError *error;

//! @brief  Currently available fruit. FCAFruit objects.
@property (strong, nonatomic, readonly) NSArray *fruit;

//! @brief  Data store state.
@property (nonatomic) FCADataStoreState state;


/*!
 @brief Shared instance.
 
 @note
 This is not a singleton instance; other instances can be created if needed.
 
 @return
 A shared data store instance.
 */
+ (FCADataStore *)sharedStore;


/*!
 @brief Load the data store from cache.
 
 @return    YES if the read was successful.
 */
- (BOOL)readFromCache;

/*!
 @brief Reload the data store.
 */
- (void)reload;


/*!
 @brief Cache the data store.
 
 @return    YES if the write was successful.
 */
- (BOOL)writeToCache;

@end
