//
//  FCADataStore.m
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 07/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import "FCADataStore.h"

#import "FCAConstants.h"
#import "FCAFruit.h"
#import "FCALogger.h"


//! @cond   Private extension.
@interface FCADataStore () <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

// @brief   RW redeclarations.
@property (strong, nonatomic, readwrite) NSError *error;
@property (strong, nonatomic, readwrite) NSArray *fruit;

// @brief   Received data.
@property (strong, nonatomic) NSMutableData *_data;

// @brief   Timestamp of reload request.
@property (nonatomic) NSTimeInterval    _reloadTimestamp;

/*
 @brief Load data into the store.
 
 @param data    Data to load.
 @param cached  Is the data from a cached store?
 
 @return    
 YES if the load has been successful. If NO, the store's error will be set.
 */
- (BOOL)loadData:(NSData *)data cached:(BOOL)cached;

@end
//! @endcond


@implementation FCADataStore

#pragma mark - Private methods

- (BOOL)loadData:(NSData *)data cached:(BOOL)cached
{
    NSError *error = nil;
    NSDictionary *rawFruit = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:&error];
    if (error) {
        self.error = error;
        return NO;
    }
    
    NSMutableArray *fruitArray = [NSMutableArray array];
    for (NSDictionary *fruitInfo in rawFruit[FCADataFruitKey]) {
        FCAFruit *fruit = [[FCAFruit alloc] initWithDictionary:fruitInfo];
        if (fruit) {
            [fruitArray addObject:fruit];
        }
    }
    self.fruit = fruitArray;
    
    return YES;
}


#pragma mark - Public methods


+ (FCADataStore *)sharedStore
{
    // Create or reuse a shared instance using GCD.
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


- (BOOL)readFromCache
{
    NSURL *containerURL = [[NSFileManager defaultManager]
                           containerURLForSecurityApplicationGroupIdentifier:FCAAppGroup];
    NSURL *fileURL = [containerURL URLByAppendingPathComponent:FCACacheFilename];
    if (! fileURL) return NO;

    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfURL:fileURL
                                         options:NSDataReadingUncached
                                           error:&error];
    
    BOOL result = [self loadData:data cached:YES];
    self.state = result ? FCADataStoreStateCached : FCADataStoreStateEmpty;
    
    return result;
}


- (void)reload
{
    // Allow one reload at once.
    if (self.state == FCADataStoreStateLoading) return;
    self.state = FCADataStoreStateLoading;

    self._reloadTimestamp = [NSDate timeIntervalSinceReferenceDate];

    NSURL *url = [NSURL URLWithString:FCADataURLString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}


- (BOOL)writeToCache
{
    NSURL *containerURL = [[NSFileManager defaultManager]
                           containerURLForSecurityApplicationGroupIdentifier:FCAAppGroup];
    NSURL *fileURL = [containerURL URLByAppendingPathComponent:FCACacheFilename];
    if (! fileURL) return NO;
    
    NSError *error = nil;
    BOOL result = [self._data writeToURL:fileURL options:NSDataWritingAtomic error:&error];
    
    NSNumber *timestamp = @([NSDate timeIntervalSinceReferenceDate]);
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:FCAAppGroup];
    [defaults setObject:timestamp forKey:FCACacheTimestampKey];
    [defaults synchronize];
    
    return result;
}


#pragma mark - NSURLConnection delegates

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.error = error;
    [FCALogger logEvent:FCALoggerEventError message:self.error.debugDescription synchronous:NO];
    self.state = FCADataStoreStateError;
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    FCADataStoreState newState = ([self loadData:self._data cached:NO] ?
                                  FCADataStoreStateReady : FCADataStoreStateError);
    NSTimeInterval loadDuration = [NSDate timeIntervalSinceReferenceDate] - self._reloadTimestamp;
    [FCALogger logEvent:FCALoggerEventLoad
                message:[NSString stringWithFormat:@"%.0f", loadDuration * 1000]
            synchronous:NO];
    
    if (newState == FCADataStoreStateError) {
        [FCALogger logEvent:FCALoggerEventError
                    message:self.error.debugDescription
                synchronous:NO];
    }
    
    self.state = newState;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self._data appendData:data];
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self._data = [[NSMutableData alloc] init];
}

@end
