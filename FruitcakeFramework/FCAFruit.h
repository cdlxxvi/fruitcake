//
//  FCAFruit.h
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 07/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import <Foundation/Foundation.h>

/*!
 @brief A representation of fruit.
 */
@interface FCAFruit : NSObject


/*!
 @brief Initialise with data dictionary.

 @note  This is the designated initialiser.
 
 @param Dictionary representation of a fruit.
 
 @return    A fruit instance or nil on error.
 
 */
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSString *formattedPrice;
@property (strong, nonatomic, readonly) NSString *formattedWeight;

@end
