//
//  FCALogger.m
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 08/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import "FCALogger.h"

#import "FCAConstants.h"


@implementation FCALogger

+ (void)logEvent:(FCALoggerEvent)event message:(NSString *)message synchronous:(BOOL)synchronous
{
    NSMutableString *urlString = [FCALoggerURLString mutableCopy];
    switch (event) {
        case FCALoggerEventDisplay:
            [urlString appendString:FCALoggerDisplayParam];
            break;
            
        case FCALoggerEventError:
            [urlString appendString:FCALoggerErrorParam];
            break;
            
        case FCALoggerEventLoad:
            [urlString appendString:FCALoggerLoadParam];
            break;
            
        default:
            break;
    }
    [urlString appendFormat:@"&data=%@", [message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    if (synchronous) {
        NSError *error = nil;
        NSURLResponse *response = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSLog(@"%@ - response: %@, error: %@", NSStringFromSelector(_cmd), response, error);
    }
    else {
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^
         (NSURLResponse *response, NSData *data, NSError *connectionError) {
             // Fire and forget - at the moment we don't care if the logging worked.
         }];
    }
}

@end
