//
//  FCAConstants.m
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 08/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import "FCAConstants.h"


NSString *const FCAAppGroup             = @"group.com.natena.Fruitcake";
NSString *const FCACacheFilename        = @"FCACache.json";
NSString *const FCACacheTimestampKey    = @"FCACacheTimestampKey";
NSString *const FCADataFruitKey         = @"fruit";
NSString *const FCADataPriceKey         = @"price";
NSString *const FCADataTypeKey          = @"type";
NSString *const FCADataURLString        = @"https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/data.json";
NSString *const FCADataWeightKey        = @"weight";
NSString *const FCALoggerDisplayParam   = @"event=display";
NSString *const FCALoggerErrorParam     = @"event=error";
NSString *const FCALoggerLoadParam      = @"event=load";
NSString *const FCALoggerURLString      = @"https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats?";