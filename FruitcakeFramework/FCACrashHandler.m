//
//  FCACrashHandler.m
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 09/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import "FCACrashHandler.h"

#import "FCALogger.h"


void HandleException(NSException *e);
void HandleSignal(int signal);


@implementation FCACrashHandler

#pragma mark - Public methods

+ (FCACrashHandler *)sharedHandler
{
    // Create or reuse a shared instance using GCD.
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


- (void)installForExceptions:(BOOL)handleExceptions signals:(NSArray *)signals
{
    if (handleExceptions) {
        NSSetUncaughtExceptionHandler(&HandleException);
    }
    
    if (signals.count > 0) {
        struct sigaction signalAction;
        memset(&signalAction, 0, sizeof(signalAction));
        signalAction.sa_handler = &HandleSignal;
        
        for (NSNumber *signal in signals) {
            int err = sigaction([signal intValue], &signalAction, NULL);
            if (err) {
                NSLog(@"Cannot set-up exception handler for signal %@. errno=%d", signal, errno);
            }
        }
    }
}   // installForExceptions:

@end


#pragma mark - Utility functions

static BOOL CrashOccurred = NO;

void HandleException(NSException *e)
{
    if (CrashOccurred) return; // Already logged a crash
    CrashOccurred = YES;
    
    // This is the swansong of the app, so log the error synchronously - otherwise
    // the process will likely be no more before the log web service call is made.
    [FCALogger logEvent:FCALoggerEventError message:e.debugDescription synchronous:YES];
}


void HandleSignal(int signal)
{
    if (CrashOccurred) return; // already logged
    
    @try {
        NSString *reason = [NSString stringWithFormat:@"%d", signal];
        NSException *e = [NSException exceptionWithName:@"Signal"
                                                 reason:reason
                                               userInfo:nil];
        [e raise];
    }
    @catch (NSException *exception) {
        HandleException(exception);
    }
    
    // Unregister the custom signal handler, allowing the OS to handle the
    // signal the normal way, most likely crashing the app. Otherwise the signal
    // would be delivered to this handler again, resulting in an infinite loop.
    struct sigaction signalAction;
    memset(&signalAction, 0, sizeof(signalAction));
    signalAction.sa_handler = SIG_DFL;
    
    int err = sigaction(signal, &signalAction, NULL);
    if (err) {
        NSLog(@"Cannot unset handler for signal %d. errno=%d", signal, errno);
    }
}
