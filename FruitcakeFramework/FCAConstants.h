//
//  FCAConstants.h
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 08/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import <Foundation/Foundation.h>


FOUNDATION_EXPORT NSString *const FCAAppGroup;
FOUNDATION_EXPORT NSString *const FCACacheFilename;
FOUNDATION_EXPORT NSString *const FCACacheTimestampKey;
FOUNDATION_EXPORT NSString *const FCADataFruitKey;
FOUNDATION_EXPORT NSString *const FCADataPriceKey;
FOUNDATION_EXPORT NSString *const FCADataTypeKey;
FOUNDATION_EXPORT NSString *const FCADataURLString;
FOUNDATION_EXPORT NSString *const FCADataWeightKey;
FOUNDATION_EXPORT NSString *const FCALoggerDisplayParam;
FOUNDATION_EXPORT NSString *const FCALoggerErrorParam;
FOUNDATION_EXPORT NSString *const FCALoggerLoadParam;
FOUNDATION_EXPORT NSString *const FCALoggerURLString;