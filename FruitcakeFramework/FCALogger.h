//
//  FCALogger.h
//  Fruitcake
//
//  Created by Jacek Błaszkowski on 08/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//


#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, FCALoggerEvent) {
    FCALoggerEventDisplay,  //! @brief  UI display.
    FCALoggerEventError,    //! @brief  Error condition.
    FCALoggerEventLoad,     //! @brief  Web service call.
};


/*!
 @brief Logging engine.
 */
@interface FCALogger : NSObject

/*!
 @brief Make a an event-logging web service call.
 
 @param event       Event type
 @param message     Message containing event details.
 @param synchronous Log the event synchronously.
 */
+ (void)logEvent:(FCALoggerEvent)event
         message:(NSString *)message
     synchronous:(BOOL)synchronous;

@end