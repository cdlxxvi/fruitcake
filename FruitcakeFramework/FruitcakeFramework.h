//
//  FruitcakeFramework.h
//  FruitcakeFramework
//
//  Created by Jacek Błaszkowski on 07/05/2015.
//  Copyright (c) 2015 Natena Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FruitcakeFramework.
FOUNDATION_EXPORT double FruitcakeFrameworkVersionNumber;

//! Project version string for FruitcakeFramework.
FOUNDATION_EXPORT const unsigned char FruitcakeFrameworkVersionString[];

#import <FruitcakeFramework/FCACrashHandler.h>
#import <FruitcakeFramework/FCADataStore.h>
#import <FruitcakeFramework/FCAFruit.h>
#import <FruitcakeFramework/FCALogger.h>


