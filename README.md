# README #

Hello, fruit lovers!

### What is this repository for? ###

* The app, predictably, displays some fruit. It is written in Swift.
* Separating the business logic into a separate framework (written in Objective-C) might seem an overkill, but...
* ... the project also contains a watch app (also written in Swift)!

### Some caveats ###

* Unit tests exist for some of the framework APIs. They are not exhaustive, but hopefully enough for the purpose of this demo.
* The crash handler catches Objective-C, but not Swift runtime exceptions.
* The coding could be more defensive in places, but then the crash handler could never get its moment in the limelight.
* The manually triggered reloads of data store with controllers KV-observing its state is not super scalable, but this app is no iPlayer.
* I did not bother with more localisation. We both know how to do it on a large scale.

### Instructions ###

* Should work OOTB in Xcode 6.3 iPhone 6 Simulator.
* When running with your own provisioning, mind the app group - without it, caching will not work (but the apps generally will).
* I am currently organising TestFlight for the app - deliver the email addresses to me and I will send the invitations.